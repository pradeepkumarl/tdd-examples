package com.classpath.service;

import com.classpath.dao.UserDAO;
import com.classpath.model.User;

public class UserService {
	
	private UserDAO userDAO;
	
	public UserService(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	public User save(User user) {
		return this.userDAO.saveUser(user);
	}
	
	

}
