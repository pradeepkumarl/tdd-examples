package com.classpath.model;

public class User {

	private int id;
	private String name;
	private Integer age;
	
	public User(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public Integer getId() {
		return this.id;
	}

	public Object getName() {
		return this.name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setId(int id) {
		this.id = id;
		
	}

	public void setName(String name) {
		this.name = name;
		
	}

	public void setAge(int age) {
		this.age = age;
		
	}

}
