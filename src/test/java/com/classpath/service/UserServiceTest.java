package com.classpath.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import com.classpath.dao.UserDAO;
import com.classpath.model.User;


public class UserServiceTest {

	
	@Test
	public void testSaveUser(@Mock UserDAO userDAO) {
		
		//set the expectation
		when(userDAO.saveUser(any(User.class)))
		.thenReturn(new User(34, "Neeraj", 33));
		
		UserService userService = new UserService(userDAO);
		User user = new User(34, "Vinay", 33);
		User savedUser = userService.save(user);
		Assertions.assertNotNull(savedUser);
	}

}
