package com.classpath.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserTests {

	@Test
	void testUserConstructor() {
		User user = new User(12, "Neeraj", 34);
		// hyposthisys
		Assertions.assertNotNull(user);
	}

	@Test
	void testUserGetterMethods() {
		User user = new User(12, "Neeraj", 34);
		Assertions.assertEquals(12, user.getId());
		Assertions.assertEquals("Neeraj", user.getName());
		Assertions.assertEquals(34, user.getAge());
	}

	@Test
	void testUserSetterMethods() {
		User user = new User(12, "Neeraj", 34);

		user.setId(14);
		user.setName("Neeraj Kumar");
		user.setAge(34);
		Assertions.assertEquals(14, user.getId());
		Assertions.assertEquals("Neeraj Kumar", user.getName());
		Assertions.assertEquals(34, user.getAge());
	}

}
